import React from "react";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import Button from "@mui/material/Button";
import Link from "./Link";
import { useRouter } from "next/router";
import { Box } from "@mui/system";
import ProfileMenuIcon from "./ProfileMenuIcon";
import { user } from "./user";

export default function Header(props: {
  mainPage?: boolean;
}) {
  const trigger = useScrollTrigger();
  const router = useRouter();

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    if (props.mainPage) {
      const scrollTo =
        document.getElementById(`${event.currentTarget.textContent}-box`)
          .offsetTop - document.getElementsByTagName("header")[0].offsetHeight;
      window.scrollTo({
        top: scrollTo,
        behavior: "smooth",
      });
    } else {
      router.push(`/#${event.currentTarget.textContent}-box`);
    }
  };

  const logOutAndGoToHomePage = () => {
    user.signOut();
    router.push("/");
  };

  return (
    <AppBar position="sticky" color="secondary">
      <Toolbar variant="regular">
        <Box sx={{ flexGrow: 1 }}>
          {["O NAS", "KURSY", "KONTAKT"].map((element) => (
            <Button color="inherit" onClick={handleClick} key={element}>
              {element}
            </Button>
          ))}
        </Box>
        <Box display={{ xs: "block", sm: "none" }}>
          <ProfileMenuIcon />
        </Box>
        <Box display={{ xs: "none", sm: "block" }}>
          {user.isSignIn() ? (
            <>
              <Box>
                <Link href="/account" underline="none" color="inherit">
                  <Button color="inherit">MOJE KONTO</Button>
                </Link>
                <Link href="/" underline="none" color="inherit">
                  <Button color="inherit" onClick={logOutAndGoToHomePage}>
                    WYLOGUJ SIĘ
                  </Button>
                </Link>
              </Box>
            </>
          ) : (
            <>
              <Link href="/signUp" underline="none" color="inherit">
                <Button color="inherit">REJESTRACJA</Button>
              </Link>
              <Link href="/signIn" underline="none" color="inherit">
                <Button color="inherit">LOGOWANIE</Button>
              </Link>
            </>
          )}
        </Box>
      </Toolbar>
    </AppBar>
  );
}
