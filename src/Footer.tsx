import React from "react";
import Copyright from "./Copyright";
import Grid from "@mui/material/Grid";

export default function Footer() {
  return (
    <Grid
      container
      alignItems="center"
      direction="row"
      justifyContent="center"
      height={60}
      bgcolor="#001"
    >
      <Copyright color="white" />
    </Grid>
  );
}
