import { LoginResponse } from "./backendObjects";

class User {
  token: string;
  name: string;
  id: number;
  email: string;

  constructor() {
    this.token = "TEST";
    this.name = "";
    this.id = 0;
    this.email = "test";
  }

  signIn = (signInData: LoginResponse) => {
    this.token = signInData.token;
    this.name = signInData.user.name;
    this.id = signInData.user.id;
    this.email = signInData.user.email;
  };

  signOut = () => {
    this.token = "";
    this.name = "";
    this.id = 0;
    this.email = "";
  };

  isSignIn = () => this.token !== "";
}

export const user = new User();
