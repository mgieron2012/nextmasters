import * as React from 'react';
import Typography from '@mui/material/Typography';
import Link from './Link';

export default function Copyright(props: any) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="/">
        Programasters
      </Link>{' '}
      2021.
    </Typography>
  );
}