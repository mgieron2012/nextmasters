import { AccountCircle } from "@mui/icons-material";
import { IconButton, Menu, MenuItem } from "@mui/material";
import router from "next/router";
import React from "react";
import Link from "./Link";
import { user } from "./user";

export default function ProfileMenuIcon() {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const logOutAndGoToHomePage = () => {
    user.signOut();
    router.push("/");
  };

  return (
    <>
      <IconButton onClick={handleMenu} color="inherit">
        <AccountCircle />
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {user.isSignIn()
          ? [
              <Link
                href="/account"
                underline="none"
                color="inherit"
                key="account"
              >
                <MenuItem>Moje konto</MenuItem>
              </Link>,
              <MenuItem onClick={() => logOutAndGoToHomePage()} key="signout">
                Wyloguj się
              </MenuItem>,
            ]
          : [
              <Link href="/signIn" underline="none" color="inherit" key="sin">
                <MenuItem>Logowanie</MenuItem>
              </Link>,
              <Link href="/signOut" underline="none" color="inherit" key="reg">
                <MenuItem>Rejestracja</MenuItem>
              </Link>,
            ]}
      </Menu>
    </>
  );
}
