import { Card, CardContent, Typography, Button, Grid } from "@mui/material";
import { useRouter } from "next/router";
import CheckCircleSharpIcon from "@mui/icons-material/CheckCircleSharp";
import React from "react";
import ButtonWithSnackbar from "./ButtonWithSnackbar";

export default function CourseCard(props: {
  title: string;
  description: string;
  url: string;
  age: string;
  price: number;
  ticks?: string[];
  id: number;
}) {
  const router = useRouter();

  return (
    <Grid item component={Card} xs>
      <CardContent>
        <Typography variant="h6" component="div" mb={3}>
          {props.title}
        </Typography>
        <Typography variant="body1" component="div">
          {props.description}
        </Typography>
        <Typography sx={{ mb: 2, mt: 2 }} color="text.secondary">
          {"Sugerowany wiek: "}
          {props.age}
        </Typography>
        <Grid container>
          <Grid xs={12} sm={8} md={12} xl={8} container item direction="row">
            {props.ticks.map((tick, it) => (
              <Grid
                container
                key={it}
                justifyContent="flex-start"
                direction="row"
                mt={1}
                alignItems="stretch"
              >
                <Grid item width={["7%", "3%", "5%", "3%", "2%"]}>
                  <CheckCircleSharpIcon color="success" width="100%" />
                </Grid>
                <Grid item width="90%">
                  <Typography ml={2}>{tick}</Typography>
                </Grid>
              </Grid>
            ))}
          </Grid>
          <Grid
            container
            item
            xs={12}
            sm={4}
            md={12}
            xl={4}
            mt={1}
            direction="column"
            p={[2, 0, 2, 2, 0]}
          >
            <Grid item container direction="row">
              <Typography fontSize={50} variant="body1">
                {props.price}
              </Typography>
              <Typography
                fontSize={30}
                variant="body1"
                sx={{ verticalAlign: "text-bottom", ml: 1 }}
              >
                PLN
              </Typography>
            </Grid>
            <Typography variant="body1" fontSize={15}>
              Całkowity koszt kursu
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
      <Grid
        item
        sx={{
          width: "100%",
          color: "white",
          backgroundColor: "secondary.light",
        }}
      >
        <Button
          sx={{
            width: "50%",
            color: "inherit",
            borderRight: "2px solid white",
            borderTopRightRadius: 0,
            borderBottomRightRadius: 0,
          }}
          onClick={() => router.push(`/course/${props.id}`)}
        >
          WYPRÓBUJ
        </Button>
        <ButtonWithSnackbar buttonText="KUP KURS" infoText="Wkrótce dostępne" sx={{ width: "50%", color: "inherit" }}/>
      </Grid>
    </Grid>
  );
}
