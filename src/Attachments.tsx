import { Attachment } from "./backendObjects";
import React, { useEffect } from "react";
import {
  List,
  ListItem,
  Grid,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
} from "@mui/material";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import VisibilityIcon from "@mui/icons-material/Visibility";

export default function Attachments(props: { data: Array<Attachment> }) {
  if (props?.data && props.data.length > 0)
    return (
      <Grid>
        <Typography variant="h5" p={1}>Materiały do lekcji </Typography>
        <List>
          {props.data.map((attachment: Attachment, idx: number) => (
            <AttachmentListItem attachment={attachment} key={idx} />
          ))}
        </List>
      </Grid>
    );
  else
    return (
      <Grid>
        <Typography>
          Do tej lekcji nie ma żadnych dodatkowych meteriałów
        </Typography>
      </Grid>
    );
}

function AttachmentListItem(props: { attachment: Attachment }) {
  const [open, setOpen] = React.useState(false);
  const onIframeLoad = (iframeElement: HTMLIFrameElement) => {
    iframeElement.style.height = `${iframeElement.parentElement.offsetHeight}px`;
  };

  const isAttachmentOpenable = ["jpg", "png", "pdf", "txt"].includes(
    props.attachment.extension
  );

  useEffect(() => {
    if (!isAttachmentOpenable) setTimeout(() => setOpen(false), 300);
  }, [open, isAttachmentOpenable]);

  return (
    <ListItem disablePadding>
      <ListItemButton onClick={() => setOpen(true)}>
        <ListItemIcon>
          {isAttachmentOpenable ? <VisibilityIcon /> : <FileDownloadIcon />}
        </ListItemIcon>
        <ListItemText primary={props.attachment.name} />
      </ListItemButton>
      {open && (
        <Dialog
          open={open}
          onClose={() => setOpen(false)}
          maxWidth="xl"
          fullWidth
          sx={{
            minHeight: "80vh",
            display: isAttachmentOpenable ? "block" : "none",
          }}
        >
          <DialogTitle>{props.attachment.name}</DialogTitle>
          <DialogContent
            sx={{ minHeight: "70vh", width: "100%", height: "100%" }}
          >
            <iframe
              src={props.attachment.url}
              width="100%"
              onLoad={(el) => onIframeLoad(el.currentTarget)}
              lang="pl-PL"
              onChange={(el) => onIframeLoad(el.currentTarget)}
            ></iframe>
            {/* height works weird here*/}
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setOpen(false)} autoFocus>
              Zamknij
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </ListItem>
  );
}
