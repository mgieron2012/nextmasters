import React, { useState } from "react";
import { user } from "./user";

export const url = "https://djangomasters-p43q4.ondigitalocean.app/api";
//export const url = "http://192.168.1.245:8000/api";

export enum ENDPOINT {
  SIGN_IN = "login",
  SIGN_UP = "register",
  VERIFY_EMAIL = "email/verify",
  COURSE = "courses",
  PROGRESS = "progress",
  COURSE_PERSONAL = "courses/private",
  LESSON = "lessons/private",
  PAYMENT = "pay",
  USER = "user",
  PASSWORD_RECOVERY = "password/recovery",
  PASSWORD_RESET = "password/reset"
}

export type METHOD = "GET" | "POST" | "PATCH" | "DELETE";

export default function useAPIFetch<DataType>(
  endpoint: ENDPOINT,
  method: METHOD = "GET"
): [
  (data: any, ...queryParams: Array<string | number>) => void,
  number,
  DataType,
  boolean
] {
  const [status, setStatus] = useState<number>(0);
  const [data, setData] = useState<DataType>();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const execute = (data: any, ...queryParams: Array<string | number>) => {
    setIsLoading(true);
    fetch(`${url}/${endpoint}/${queryParams.join("/")}`, {
      method: method,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.token}`,
      },
      body: data !== undefined ? JSON.stringify(data) : data,
    })
      .then((response) => {
        setStatus(response.status);
        return response.json();
      })
      .then((result) => {
        setData(result);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error("Error:", error);
        setIsLoading(false);
      });
  };

  return [execute, status, data, isLoading];
}

export function simpleAPIFetch<DataType>(
  endpoint: ENDPOINT,
  method: METHOD = "GET",
  data: any,
  ...queryParams: Array<string | number>
): { status: number; data: DataType } {
  let status: number;
  let responseData: DataType;

  fetch(`${url}/${endpoint}/${queryParams.join("/")}`, {
    method: method,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${user.token}`,
    },
    body: data !== undefined ? JSON.stringify(data) : data,
  })
    .then((response) => {
      status = response.status;
      return response.json();
    })
    .then((result) => {
      responseData = result;
    })
    .catch((error) => {
      console.error("Error:", error);
    });

  return {
    status: status,
    data: responseData,
  };
}
