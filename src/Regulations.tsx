import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import React from "react";

export default function RegulationsButton() {
  const [open, setOpen] = React.useState(false);

  return (
    <>
      <Button variant="text" onClick={() => setOpen(true)}>
        regulamin
      </Button>
      {open && (
        <Dialog
          open={open}
          onClose={() => setOpen(false)}
        >
          <DialogTitle>
            Regulamin
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              Coś tu się wymyśli
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setOpen(false)} autoFocus>
              Zgadzam się
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </>
  );
}
