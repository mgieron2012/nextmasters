export type Lesson = {
  id: number;
  number: number;
  name: string;
};

export type Module = {
  id: number;
  number: number;
  name: string;
  lessons: Array<Lesson>;
};

export type Course = {
  id: number;
  name: string;
  modules: Array<Module>;
};

export type Progress = {
  id: number;
  elapsed_seconds: number;
  is_lesson_finished: boolean;
};

export type Attachment = {
  id: number;
  name: string;
  url: string;
  extension: string;
};

export type Product = {
  id: number;
  price: number;
  course: {
    id: number;
    name: string;
  }
}

export type Purchase = {
  id: number;
  stripe_id: string;
  product: Product;
  is_done: boolean;
  is_paid: boolean;
  created: string;
}

export type CoursePersonalResponse = {
  is_bought: boolean;
  next_lesson: {
    id: number;
    module_id: number;
  } | null;
};

export type LessonPersonalResponse = {
  url: string;
  attachments: Array<Attachment>;
  progress: Progress | null;
  name: string;
};

export type LoginResponse = {
  token: string;
  user: {
    name: string;
    id: number;
    email: string;
  };
};

export type UserDetails = {
  id: number;
  name: string;
  email: string;
  purchases: Array<Purchase>;
}