import { Button, CircularProgress, Grid } from "@mui/material";
import React, { useRef } from "react";
import Attachments from "./Attachments";
import { LessonPersonalResponse } from "./backendObjects";
import useAPIFetch, { ENDPOINT, url } from "./useFetch";
import { user } from "./user";
import Link from "./Link";

export default function Player(props: { lessonId: number }) {
  const [executeFetch, status, fetchData, loading] =
    useAPIFetch<LessonPersonalResponse>(ENDPOINT.LESSON);

  const videoRef = useRef<HTMLVideoElement>();

  const handleVideoLoadStart = (
    video: React.SyntheticEvent<HTMLVideoElement, Event>
  ) => {
    if (fetchData?.progress)
      video.currentTarget.currentTime = fetchData.progress.elapsed_seconds;
  };

  React.useEffect(() => {
    executeFetch(undefined, props.lessonId);
  }, [props.lessonId]);

  React.useEffect(() => {
    const id = setInterval(() => {
      if (user.isSignIn && videoRef?.current && !videoRef.current.paused) {
        fetch(`${url}/${ENDPOINT.PROGRESS}/`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${user.token}`,
          },
          body: JSON.stringify({
            elapsed_seconds: videoRef.current.currentTime,
            is_lesson_finished:
              videoRef.current.currentTime / videoRef.current.duration > 0.9,
            lesson: props.lessonId,
          }),
        });
      }
    }, 5000);

    return () => clearInterval(id);
  }, [props.lessonId]);

  return (
    <Grid>
      <Grid
        container
        item
        justifyContent="center"
        alignItems="flex-start"
        sx={{
          background:
            "linear-gradient(200deg, rgba(0,0,0,1) 10%, rgba(8,30,34,1) 40%, rgba(15,62,70,1) 60%, rgba(23,136,159,1) 100%)",
          paddingTop: "56.25%",
        }}
        width="100%"
        direction="row"
        position="relative"
      >
        <Grid
          item
          container
          justifyContent="center"
          alignItems="center"
          position="absolute"
          top={0}
          left={0}
          bottom={0}
          right={0}
        >
          {loading ? (
            <CircularProgress />
          ) : status === 200 ? (
            <video
              controls
              width="100%"
              height="100%"
              ref={videoRef}
              onLoadStart={(video) => handleVideoLoadStart(video)}
              controlsList="nodownload"
            >
              <source src={fetchData.url} type="video/mp4" />
            </video>
          ) : status === 454 ? (
            <Link href={user.isSignIn() ? `${url}/${ENDPOINT.PAYMENT}?lesson=${props.lessonId}&email=${user.email}` : '/signIn'} underline="none">
              <Button
                sx={{
                  fontSize: 20,
                  backgroundColor: "black",
                  border: "1px solid rgb(23,136,159)",
                  color: "rgb(23,136,159)",
                  ":hover": {
                    backgroundColor: "rgb(23,136,159)",
                    color: "black",
                  },
                }}
              >
                Kup kurs
              </Button>
            </Link>
          ) : status === 403 || status === 453 ? (
            <Link href="/signIn?goBack=1" underline="none">
              <Button
                sx={{
                  fontSize: 20,
                  backgroundColor: "black",
                  border: "1px solid rgb(23,136,159)",
                  color: "rgb(23,136,159)",
                  ":hover": {
                    backgroundColor: "rgb(23,136,159)",
                    color: "black",
                  },
                }}
              >
                Kup kurs
              </Button>
            </Link>
          ) : (
            <>Nieoczekiwany błąd</>
          )}
        </Grid>
      </Grid>
      <Grid>{fetchData && <Attachments data={fetchData?.attachments} />}</Grid>
    </Grid>
  );
}
