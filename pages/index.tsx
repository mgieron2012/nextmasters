import * as React from "react";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import ProTip from "../src/ProTip";
import Link from "../src/Link";
import Copyright from "../src/Copyright";
import Header from "../src/Header";
import { Divider, Grid } from "@mui/material";
import CourseCard from "../src/CourseCard";
import Footer from "../src/Footer";

export default function Index() {
  return (
    <Grid container>
      <Header mainPage />
      <Grid
        item
        xs={12}
        height="100vh"
        sx={{
          backgroundImage: `url(mc5.jpg)`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
        color="white"
      >
        <Grid container alignItems="center" direction="column" width="100%">
          <Grid
            container
            alignItems="stretch"
            alignContent="flex-end"
            height="40vh"
            width="100%"
            mb="2vh"
          >
            <Typography
              component="h2"
              color="inherit"
              fontWeight={800}
              letterSpacing={5}
              fontSize={[30, 45, 60, 80, 70]}
              textAlign="center"
              width="100%"
            >
              Programasters
            </Typography>
          </Grid>
          <Divider
            variant="inset"
            sx={{
              borderColor: "gray",
              borderWidth: 2,
              width: "70%",
              m: 0,
              p: 0,
            }}
          />
          <Typography
            component="h4"
            color="inherit"
            fontWeight={400}
            letterSpacing={2.5}
            fontSize={[15, 25, 30, 40, 45]}
            textAlign="center"
            mt="1vh"
          >
            Kursy programowania dla dzieci
          </Typography>
        </Grid>
      </Grid>
      <Grid
        id="KURSY-box"
        minHeight="100vh"
        direction="row"
        container
        justifyContent="center"
        alignItems="center"
      >
        <Grid item xs={12}>
          <Typography
            variant="h3"
            color="gray"
            letterSpacing={10}
            textAlign="center"
            p={2}
            fontSize={[30, 40]}
          >
            NASZE KURSY
          </Typography>
          <Divider
            variant="inset"
            sx={{
              color: "gray",
              width: "100%",
              m: 0,
              p: 0,
            }}
          />
        </Grid>
        <Grid
          item
          xs={12}
          md={6}
          p="2vh"
          m={0}
          sx={{ height: ["auto", "auto", "100%"] }}
        >
          <CourseCard
            title="Python - podstawy"
            description="Każdy uczestnik kursu Python - Podstawy nauczy się podstaw programowania, które będą solidnym fundamentem do nauki każdego innego języka programowania, a także do wszystkich innych kursów z programowania na naszej stronie. Język Python posiada bardzo prostą składnię, co niezwykle ułatwia jego opanowanie i jest wręcz idealny, żeby rozpocząć swoją przygodę z kodowaniem. Dzięki niemu, uczestnik kursu pozna wszystkie podstawowe mechanizmy, używane podczas pisania programów, takie jak zmienne, pętle lub instrukcje warunkowe. Wiele przykładów z kursu nawiązuje do gier komputerowych znanych, co jeszcze bardziej ułatwia zrozumienie tych zagadnień. Nie zabraknie tutaj również elementów grafiki, które realizowane będzie za pomocą żółwika, który podobny jest do tego z programu Logomocja."
            age="10+ lat"
            url="/python1"
            price={199}
            ticks={[
              "50 lekcji",
              "Ponad 30 godzin materiałów wideo",
              "Podsumowania lekcji w formie prezentacji",
              "Zadania do samodzielnego wykonania",
            ]}
            id={1}
          ></CourseCard>
        </Grid>
        <Grid
          item
          xs={12}
          md={6}
          p="2vh"
          m={0}
          sx={{ height: ["auto", "auto", "100%"] }}
        >
          <CourseCard
            title="Minecraft"
            description="Każdy uczestnik kursu Python 1 - Podstawy nauczy się podstaw programowania, które będą solidnym fundamentem do nauki każdego innego języka programowania, a także do wszystkich innych kursów z programowania na naszej stronie. Język Python posiada bardzo prostą składnię, co niezwykle ułatwia jego opanowanie i jest wręcz idealny, żeby rozpocząć swoją przygodę z kodowaniem. Dzięki niemu, uczestnik kursu pozna wszystkie podstawowe mechanizmy, używane podczas pisania programów, takie jak zmienne, pętle lub instrukcje warunkowe. Wiele przykładów z kursu nawiązuje do gier komputerowych znanych, co jeszcze bardziej ułatwia zrozumienie tych zagadnień. Nie zabraknie tutaj również elementów grafiki, które realizowane będzie za pomocą żółwika, który podobny jest do tego z programu Logomocja."
            age="10+ lat"
            url="/minecraft1"
            price={249}
            ticks={[
              "30 lekcji",
              "Ponad 20 godzin materiałów wideo",
              "Podsumowania lekcji w formie prezentacji",
            ]}
            id={2}
          ></CourseCard>
        </Grid>
      </Grid>
      <Grid id="O NAS-box" xs={12} item height="100vh">
        O NAS
      </Grid>
      <Grid id="KONTAKT-box" xs={12} item height="100vh">
        KONTAKT
      </Grid>
      <Footer />
    </Grid>
  );
}
