import * as React from "react";
import Avatar from "@mui/material/Avatar";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Link from "../src/Link";
import Copyright from "../src/Copyright";
import theme from "../src/theme";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import { ThemeProvider } from "@mui/material/styles";
import useAPIFetch, { ENDPOINT } from "../src/useFetch";
import router from "next/router";
import Checkbox from "@mui/material/Checkbox";
import RegulationsButton from "../src/Regulations";
import LoadingButton from "@mui/lab/LoadingButton";
import { user } from "../src/user";
import { LoginResponse } from "../src/backendObjects";

export default function SignUp() {
  const [executeFetch, status, fetchData, loading] = useAPIFetch<LoginResponse>(
    ENDPOINT.SIGN_UP,
    "POST"
  );

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    executeFetch(Object.fromEntries(data.entries()));
  };

  React.useEffect(() => {
    if (!loading && status == 200) {
      user.signIn(fetchData);
      router.push('/confirmEmail');
    }
  }, [loading, status]);

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: "url(/mc2.png)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Rejestracja
            </Typography>
            <Box component="form" onSubmit={handleSubmit} sx={{ mt: 3 }}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    name="name"
                    required
                    fullWidth
                    id="firstName"
                    label="Imię"
                    autoFocus
                    inputProps={{ minLength: 3 }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="email"
                    label="Adres email"
                    name="email"
                    error={[450, 451].includes(status)}
                    helperText={
                      status === 450
                        ? "Konto z podany emailem już istnieje"
                        : status === 451
                        ? "Nieprawidłowy email"
                        : ""
                    }
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="password"
                    label="Hasło"
                    type="password"
                    id="password"
                    inputProps={{ minLength: 8 }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Checkbox value="regulations" color="primary" required />
                  Akceptuję <RegulationsButton />
                </Grid>
              </Grid>
              <LoadingButton
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                loading={loading}
              >
                Załóż konto
              </LoadingButton>
              <Grid container justifyContent="flex-end">
                <Grid item>
                  <Link href="/signIn" variant="body2">
                    Masz już konto? Zaloguj się
                  </Link>
                </Grid>
              </Grid>
            </Box>
          </Box>
          <Copyright sx={{ mt: 5 }} />
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
