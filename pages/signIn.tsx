import * as React from "react";
import Avatar from "@mui/material/Avatar";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Link from "../src/Link";
import Copyright from "../src/Copyright";
import theme from "../src/theme";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import { ThemeProvider } from "@mui/material/styles";
import useAPIFetch, { ENDPOINT } from "../src/useFetch";
import { useRouter } from "next/router";
import LoadingButton from "@mui/lab/LoadingButton";
import { LoginResponse } from "../src/backendObjects";
import { user } from "../src/user";

export default function SignIn() {
  const router = useRouter();
  const { goBack } = router.query;

  const [executeFetch, status, fetchData, loading] = useAPIFetch<LoginResponse>(
    ENDPOINT.SIGN_IN,
    "POST"
  );

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    executeFetch(Object.fromEntries(data.entries()), undefined);
  };

  React.useEffect(() => {
    if (!loading && status == 200) {
      user.signIn(fetchData);
      if(goBack === "1"){
        router.back();
      } else {
        router.push('/');
      }
    }
  }, [loading, status]);

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: `url(mc1.png)`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Logowanie
            </Typography>
            <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label="Adres email"
                name="email"
                type="email"
                autoFocus
                error={status === 452}
                helperText={status === 452 ? "Nieporawny email lub hasło" : ""}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label="Hasło"
                type="password"
                id="password"
                error={status === 452}
                helperText={status === 452 ? "Nieporawny email lub hasło" : ""}
              />
              <LoadingButton
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                loading={loading}
              >
                Zaloguj się
              </LoadingButton>
              <Grid container>
                <Grid item xs>
                  <Link href="/passwordRecovery" variant="body2">
                    Zapomniałeś hasło?
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="/signUp" variant="body2">
                    {"Zarejestruj się"}
                  </Link>
                </Grid>
              </Grid>
              <Copyright sx={{ mt: 5 }} />
            </Box>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
