import * as React from "react";
import Avatar from "@mui/material/Avatar";
import CssBaseline from "@mui/material/CssBaseline";
import Link from "../src/Link";
import theme from "../src/theme";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import EmailIcon from "@mui/icons-material/Email";
import Typography from "@mui/material/Typography";
import { ThemeProvider } from "@mui/material/styles";
import useAPIFetch, { ENDPOINT } from "../src/useFetch";
import { useRouter } from "next/router";

export default function ConfirmEmail() {
  const router = useRouter();
  const { token } = router.query;

  const [executeFetch, status, fetchData, loading] = useAPIFetch(
    ENDPOINT.VERIFY_EMAIL,
    "GET"
  );

  const checkToken = () => {
    executeFetch(undefined, token as string);
  };

  React.useEffect(() => {
    if (token !== undefined) {
      checkToken();
    }
  }, [token]);

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: `url(mc3.png)`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
              <EmailIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Potwierdź email
            </Typography>
            {token === undefined ? (
              <Typography
                component="h3"
                variant="h6"
                align="center"
                sx={{ mt: 10 }}
              >
                Wysłaliśmy do Ciebie maila weryfikacyjnego.
                <br /> Sprawdź swoją skrzynkę pocztową!
              </Typography>
            ) : loading || status == 200 ? (
              <>
                <Typography
                  component="h3"
                  variant="h4"
                  align="center"
                  sx={{ mt: 10, mb: 5 }}
                >
                  Udało się!
                </Typography>
                <Link href="/" underline="none">
                  <Button variant="contained" color="success" size="large"> Powrót do strony głównej</Button>
                </Link>
              </>
            ) : (
              <Typography
                component="h3"
                variant="h6"
                align="center"
                sx={{ mt: 10 }}
              >
                Wystąpił błąd
              </Typography>
            )}
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
