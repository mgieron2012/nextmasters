/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import VideoPlayer from "../../src/Player";
import { GetStaticProps } from "next";
import useAPIFetch, { ENDPOINT, url } from "../../src/useFetch";
import {
  Button,
  Collapse,
  Grid,
  List,
  ListItemButton,
  ListItemText,
} from "@mui/material";
import {
  ExpandLess,
  ExpandMore,
  ArrowLeft,
  ArrowRight,
} from "@mui/icons-material";
import {
  Course,
  CoursePersonalResponse,
  Lesson,
  Module,
} from "../../src/backendObjects";
import Header from "../../src/Header";

export default function CoursePage(props: { course: Course }) {
  const [currentModule, setCurrentModule] = React.useState<number>(
    props.course.modules[0].id
  );

  const [currentLesson, setCurrentLesson] = React.useState<number>(
    props.course.modules[0].lessons[0].id
  );

  const [executeFetch, status, fetchData, loading] =
    useAPIFetch<CoursePersonalResponse>(ENDPOINT.COURSE_PERSONAL, "GET");

  useEffect(() => {
    executeFetch(undefined, props.course.id);
  }, []);

  useEffect(() => {
    if (status === 200 && fetchData.next_lesson !== null) {
      setCurrentLesson(fetchData.next_lesson.id);
      setCurrentModule(fetchData.next_lesson.module_id);
    }
  }, [fetchData]);

  return (
    <Grid container>
      <Header></Header>
      <Grid
        item
        xs={12}
        md={8}
        lg={9}
        xl={8}
        sx={{ padding: { xs: 1, xl: 3 } }}
      >
        <Grid container>
          <Grid item xs={2}>
            <Button onClick={() => {}} startIcon={<ArrowLeft />}></Button>
          </Grid>
          <Grid item xs={8}></Grid>
          <Grid item xs={2}>
            <Button onClick={() => {}} endIcon={<ArrowRight />}></Button>
          </Grid>
        </Grid>
        <VideoPlayer lessonId={currentLesson}></VideoPlayer>
      </Grid>
      <Grid
        item
        xs={12}
        md={4}
        lg={3}
        xl={4}
        maxHeight="70vh"
        overflow="auto"
        sx={{ padding: { xs: 1, xl: 3 } }}
      >
        <List component="nav">
          {props.course.modules.map((module: Module) => (
            <>
              <ListItemButton
                onClick={() =>
                  setCurrentModule(currentModule === module.id ? 0 : module.id)
                }
                key={module.id}
              >
                <ListItemText primary={module.name} />
                {module.id === currentModule ? <ExpandLess /> : <ExpandMore />}
              </ListItemButton>
              <Collapse
                in={module.id === currentModule}
                timeout="auto"
                unmountOnExit
              >
                <List component="div" disablePadding>
                  {module.lessons.map((lesson: Lesson) => (
                    <ListItemButton
                      sx={{ pl: 4 }}
                      key={lesson.id}
                      onClick={() => setCurrentLesson(lesson.id)}
                      selected={currentLesson === lesson.id}
                    >
                      <ListItemText primary={lesson.name} />
                    </ListItemButton>
                  ))}
                </List>
              </Collapse>
            </>
          ))}
        </List>
      </Grid>
    </Grid>
  );
}

export async function getStaticPaths() {
  return {
    paths: [{ params: { id: "1" } }, { params: { id: "2" } }],
    fallback: false,
  };
}

export const getStaticProps: GetStaticProps = async (context) => {
  const course = await (
    await fetch(`${url}/${ENDPOINT.COURSE}/${context.params.id}/`)
  ).json();

  return {
    props: { course: course },
  };
};
