import React from "react";
import { GetServerSideProps } from "next";
import { url, ENDPOINT } from "../src/useFetch";
import { user } from "../src/user";
import { UserDetails } from "../src/backendObjects";
import Grid from "@mui/material/Grid";
import Header from "../src/Header";
import Footer from "../src/Footer";
import {
  Alert,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import Link from "../src/Link";
import { useRouter } from "next/router";

export default function Account(props: { data: UserDetails }) {
  const router = useRouter();
  const { payment } = router.query;

  const handlePasswordReset = () => {};

  return (
    <Grid
      container
      minHeight="100vh"
      direction="column"
      justifyContent="space-between"
    >
      <Header />
      <Grid container justifyContent="center">
        <Grid container item xs={12} sm={10} lg={8}>
          {payment === "0" && (
            <Alert severity="error" sx={{ width: "100%", mb: 1 }}>
              Płatność anulowana
            </Alert>
          )}
          {payment === "1" && (
            <Alert severity="success" sx={{ width: "100%", mb: 1 }}>
              Płatność zakończona
            </Alert>
          )}
          <Paper sx={{ width: "100%", fontSize: 18 }}>
            <Grid item p={3} borderBottom="2px solid gray" fontSize={30}>
              Moje dane
            </Grid>
            <Grid item p={1} ml={2} mt={2}>
              Imię: <b>{props.data.name}</b>
            </Grid>
            <Grid p={1} ml={2}>
              Email: <b>{props.data.email}</b>
            </Grid>
            <Grid p={1} ml={2}>
              Hasło: <b>●●●●●●●●●</b>
              <LoadingButton
                onClick={handlePasswordReset}
                sx={{ marginLeft: 2 }}
              >
                Zresetuj hasło
              </LoadingButton>
            </Grid>
            <Grid
              item
              p={3}
              borderBottom="2px solid gray"
              borderTop="2px solid gray"
              fontSize={30}
              mt={3}
            >
              Moje płatności
            </Grid>
            {props.data.purchases.length == 0 ? (
              <Grid item p={1} m={2}>
                Nie masz jeszcze żadnych płatności.
              </Grid>
            ) : (
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Kurs</TableCell>
                    <TableCell align="right">Cena</TableCell>
                    <TableCell align="right">Stan płatności</TableCell>
                    <TableCell align="right">Data</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {props.data.purchases.map((purchase) => (
                    <TableRow
                      key={purchase.id}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell>
                        <Link href={`/course/${purchase.product.course.id}`}>
                          {purchase.product.course.name}
                        </Link>
                      </TableCell>
                      <TableCell align="right">
                        {purchase.product.price / 100} PLN
                      </TableCell>
                      <TableCell align="right">
                        {purchase.is_paid
                          ? "Zapłacono"
                          : purchase.is_done
                          ? "Odrzucono"
                          : "W toku"}
                      </TableCell>
                      <TableCell align="right">
                        {new Date(purchase.created).toLocaleString()}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            )}
          </Paper>
        </Grid>
      </Grid>
      <Footer />
    </Grid>
  );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  if (!user.isSignIn) {
    return {
      redirect: {
        destination: "/signIn",
        permanent: false,
      },
    };
  }

  const userData = await (
    await fetch(`${url}/${ENDPOINT.USER}/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    })
  ).json();

  return {
    props: { data: userData },
  };
};
