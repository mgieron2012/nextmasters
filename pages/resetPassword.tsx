import * as React from "react";
import Avatar from "@mui/material/Avatar";
import CssBaseline from "@mui/material/CssBaseline";
import Link from "../src/Link";
import theme from "../src/theme";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import LockIcon from "@mui/icons-material/Lock";
import Typography from "@mui/material/Typography";
import { ThemeProvider } from "@mui/material/styles";
import useAPIFetch, { ENDPOINT } from "../src/useFetch";
import { useRouter } from "next/router";
import { LoadingButton } from "@mui/lab";
import { Alert, TextField } from "@mui/material";
import Copyright from "../src/Copyright";
import { user } from "../src/user";
import { LoginResponse } from "../src/backendObjects";

export default function ResetPassword() {
  const router = useRouter();
  const { token } = router.query;

  const [executeFetch, status, fetchData, loading] = useAPIFetch<LoginResponse>(
    ENDPOINT.PASSWORD_RESET,
    "POST"
  );

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    executeFetch(Object.fromEntries(data.entries()), undefined);
  };

  React.useEffect(() => {
    if (!loading && status == 200) {
      user.signIn(fetchData);
    }
  }, [loading, status, fetchData]);

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: `url(mc3.png)`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
              <LockIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Zmiana hasła
            </Typography>
            {status === 453 ? (
              <Alert severity="error" sx={{mt: 10, width: "100%"}}>Link zmiany hasła wygasł.</Alert>
            ) : [400, 404, 500].includes(status) ? (
              <Alert severity="error" sx={{mt: 10, width: "100%"}}>Wystąpił błąd.</Alert>
            ) : status === 200 ? (
              <Box component="div">
                <Alert severity="success" sx={{mt: 10, width: "100%"}}>Hasło zmienione!</Alert>
                <Link href="/" underline="none">
                  <Button variant="contained" color="success" size="large"> Powrót do strony głównej</Button>
                </Link>
              </Box>
            ) : (
              <Box component="form" onSubmit={handleSubmit} sx={{ mt: 10 }}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="password"
                  label="Nowe hasło"
                  name="password"
                  type="password"
                  autoFocus
                  inputProps={{ minLength: 6 }}
                />
                <LoadingButton
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                  loading={loading}
                >
                  Ustaw nowe hasło
                </LoadingButton>
                <Copyright sx={{ mt: 5 }} />
              </Box>
            )}
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
